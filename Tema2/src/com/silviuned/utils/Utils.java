package com.silviuned.utils;

import java.util.ArrayList;
import java.util.List;

public class Utils {

	public static List<Integer> getMissingGenes(int[][] input) {
		
		List<Integer> missingGenes = new ArrayList<>();
		for (int i = 1; i <= input.length; i++) {
			for (int j = 1; j <= input.length; j++) {
				missingGenes.add(i);
			}
		}
		
		// Eliminam genele deja existente
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input.length; j++)
			{
				if (input[i][j] != 0) {
					missingGenes.remove(new Integer(input[i][j]));
				}
			}
		}

		return missingGenes;
	}
}
