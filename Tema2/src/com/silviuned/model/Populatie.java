package com.silviuned.model;

import static com.silviuned.utils.Configs.*;
import static com.silviuned.utils.Utils.getMissingGenes;

import java.util.List;

import com.silviuned.service.DataFetcher;


public class Populatie {

	private int bestFitness;
	private Cromozom bestSolution;
	
	private int[] fitness;
	private Cromozom[] cromozomi;
	
	private int[][] input;
	
	public Populatie() {
		input = DataFetcher.getInput();
		cromozomi = new Cromozom[NR_CROMOZOMI];
		fitness = new int[NR_CROMOZOMI];
		
		List<Integer> missingGenes = getMissingGenes(input);
		for (int i = 0; i < NR_CROMOZOMI; i++) {
			cromozomi[i] = new Cromozom(input, missingGenes);
		}
	}
	
	public void evaluare() {
		bestFitness = 0;
		for (int i = 0; i < NR_CROMOZOMI; i++) {
			fitness[i] = cromozomi[i].getFitness();
			if (fitness[i] > bestFitness) {
				bestFitness = fitness[i];
				bestSolution = cromozomi[i];
			}
		}
	}
	
	public void turneu(){
		Cromozom[] populatieNoua = new Cromozom[NR_CROMOZOMI];
		for (int i = 0; i < NR_CROMOZOMI; i++){
			int randOne = (int)(Math.random() * NR_CROMOZOMI);
			int randTwo = (int)(Math.random() * NR_CROMOZOMI);
			
			if (randOne == NR_CROMOZOMI) {
				randOne--;
			}
			if (randTwo == NR_CROMOZOMI) {
				randTwo--;
			}
			
			if (fitness[randOne] >= fitness[randTwo]){
				populatieNoua[i] = new Cromozom(cromozomi[randOne].getGenes());
			} else {
				populatieNoua[i] = new Cromozom(cromozomi[randTwo].getGenes());	
			}
		}
		
		this.cromozomi = populatieNoua;
	}
	
	public void roataNorocului() {
		int fitnessTotal = 0;
		for (int f : fitness) {
			fitnessTotal += f;
		}
		
		double sumaFitness = 0;
		double[] fitnessCumulat = new double[NR_CROMOZOMI];
		for (int i = 0; i < NR_CROMOZOMI; i++){
			sumaFitness = sumaFitness + (fitness[i] * 1.0 / fitnessTotal);
			fitnessCumulat[i] = sumaFitness;
		}
		
		/* Creare generatie noua. */
		Cromozom[] populatieNoua = new Cromozom[NR_CROMOZOMI];
		for (int i = 0; i < NR_CROMOZOMI; i++) {
			double rand = Math.random();
			
			int j = 0;
			while (rand > fitnessCumulat[j]) {
				j++;
			}
			
			if (j >= NR_CROMOZOMI) {
				j = NR_CROMOZOMI - 1;
			}
			
			populatieNoua[i] = new Cromozom(cromozomi[j].getGenes());
		}
		
		this.cromozomi = populatieNoua;
	}
	
	public void incrucisare() {
		for (int i = 0; i < NR_CROMOZOMI; i++) {
			if (Math.random() < RATA_INCRUCISARE) {
				int j = (int) Math.random() * NR_CROMOZOMI;
				if (j == NR_CROMOZOMI) {
					j--;
				}
				
				if (i != j) {
					cromozomi[i].incrucisare(cromozomi[j]);
				}
			}
		}
	}
	
	public void mutatie() {
		for (int i = 0; i < NR_CROMOZOMI; i++) {
			if (Math.random() < RATA_MUTATIE) {
				int[] posOne = getMutationPositon();
				int[] posTwo = getMutationPositon();
				
				cromozomi[i].mutatie(posOne[0], posOne[1], posTwo[0], posTwo[1]);
			}
		}
	}
	
	public int[] getMutationPositon() {
		int i = -1;
		int j = -1;
		
		while ((i == -1) || input[i][j] != 0) {
			i = (int) (Math.random() * input.length);
			j = (int) (Math.random() * input.length);
			
			if (i == input.length) {
				i--;
			}
			if (j == input.length) {
				j--;
			}
		}
		
		int[] result = new int[2];
		result[0] = i;
		result[1] = j;
		return result;
	}
	
	public int getBestErrorCount() {
		int maxErrors = 3 * bestSolution.getGenes().length * bestSolution.getGenes().length;
		return maxErrors - bestFitness;
	}
	
	public Cromozom getBestSolution() {
		return bestSolution;
	}
}
