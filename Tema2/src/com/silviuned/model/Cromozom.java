package com.silviuned.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class Cromozom {

	private int[][] genes;
	
	public Cromozom(int[][] input, List<Integer> missingGenes) {
		Collections.shuffle(missingGenes);
		
		genes = new int[input.length][input.length];
		int missingGenesIterator = 0;
		
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input.length; j++) {
				if (input[i][j] != 0) {
					genes[i][j] = input[i][j];
				} else {
					genes[i][j] = missingGenes.get(missingGenesIterator);
					missingGenesIterator++;
				}
			}
		}
	}
	
	public Cromozom(int[][] genes) {
		this.genes = new int[genes.length][genes.length];
		for (int i = 0; i < genes.length; i++) {
			for (int j = 0; j < genes.length; j++) {
				this.genes[i][j] = genes[i][j];
			}
		}
	}
	
	public int getFitness() {
		int totalError = 0;
		HashSet<Integer> numberPool = new HashSet<>();
		
		/* Calcularea erorilor pe randuri */
		for (int i = 0; i < genes.length; i++) {
			numberPool.clear();
			
			for (int j = 0; j < genes.length; j++) {
				numberPool.add(genes[i][j]);
			}
			
			totalError += genes.length - numberPool.size();
		}
		
		/* Calcularea erorilor pe coloane */
		for (int j = 0; j < genes.length; j++) {
			numberPool.clear();
			
			for (int i = 0; i < genes.length; i++) {
				numberPool.add(genes[i][j]);
			}
			
			totalError += genes.length - numberPool.size();
		}
		
		/* Calcularea erorilor in sub-patrate */
		int k = (int) Math.sqrt(genes.length);
		for (int q = 0; q < k; q++) {
			for (int p = 0; p < k; p++) {
				numberPool.clear();
				
				for (int i = 0; i < k; i++) {
					for (int j = 0; j < k; j++) {
						numberPool.add(genes[q * k + i][p * k + j]);
					}
				}
				
				totalError += genes.length - numberPool.size();
			}
		}
		
		int maxErrors = 3 * genes.length * genes.length;
		return maxErrors - totalError;
	}
	
	public void incrucisare(Cromozom other) {
		int nrValues = genes.length * genes.length;
		int pos = (int) (Math.random() * nrValues);
		if (pos == nrValues) {
			pos--;
		}
		
		for (int i = pos; i < nrValues; i++) {
			genes[i / genes.length][i % genes.length] = other.getGenes()[i / genes.length][i % genes.length];
		}
	}
	
	public void mutatie(int i1, int j1, int i2, int j2) {
		int k = genes[i1][j1];
		genes[i1][j1] = genes[i2][j2];
		genes[i2][j2] = k;
	}
	
	public int[][] getGenes() {
		return this.genes;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < genes.length; i++) {
			for (int j = 0; j < genes.length; j++) {
				sb.append(genes[i][j]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	
	
}
