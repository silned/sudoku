package com.silviuned.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.silviuned.utils.Configs;

public class DataFetcher {

	public static int[][] getInput() {
		
		int[][] input = null;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(Configs.INPUT_FILE));
			
			String line = br.readLine();
			int inputSize = Integer.valueOf(line);
			input = new int[inputSize][inputSize];
			
			for (int i = 0; i < inputSize; i++) {
				line = br.readLine();
				for (int j = 0; j < inputSize; j++) {
		        	input[i][j] = Character.getNumericValue(line.charAt(j));
		        }
			}
			
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return input;
	}
}
