package com.silviuned.service;

import com.silviuned.model.Populatie;
import com.silviuned.utils.Configs;

public class AlgGenetic {
	
	public static void run(){
		long startTime = System.currentTimeMillis();
			
		Populatie populatie = new Populatie();
		populatie.evaluare();
		
		int nrGeneratie = 0;
		int minErrorCount = Integer.MAX_VALUE;
			
	    while (nrGeneratie < Configs.NR_MAX_GENERATII){
	    	nrGeneratie++;
	    	
	    	//Selectia
	    	//populatie.roataNorocului();
	    	populatie.turneu();
	    	
	    	//Incrucisare
	    	populatie.incrucisare();
	    	
	    	//Mutatie
	    	populatie.mutatie();
	    	
	    	//Evaluare
	    	populatie.evaluare();
	    	
	    	int errorCount = populatie.getBestErrorCount();
	    	if (errorCount < minErrorCount) {
	    		minErrorCount = errorCount;
	    		//System.out.println("Errors: " + errorCount);
	    		//System.out.println("Generation: " + nrGeneratie);
	    	}
	    	if (errorCount == 0) {
	    		break;
	    	}
	    }
	    
	    long executionTime = System.currentTimeMillis() - startTime;
	    System.out.println("Timp de executie:\t" + executionTime);
		
	    if (minErrorCount == 0) {
	    	System.out.println("S-a gasit solutia in " + nrGeneratie + " generatii.");
	    	System.out.println(populatie.getBestSolution());
	    } else {
	    	System.out.println("Nu s-a gasit o solutie in " + Configs.NR_MAX_GENERATII + " generatii.");
	    	System.out.println("Eroarea minima: " + minErrorCount);
	    }
	    System.out.println();
	}
	
}
